import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { IpServiceService } from 'src/app/services/ip-service.service';
import { LocationService } from 'src/app/services/location.service';
import { ForecastService } from 'src/app/services/forecast.service';

@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.scss']
})
export class LocationComponent implements OnInit {
  ipAddress: string;
  ubicationData: [];
  dataCharge: boolean = false;

  dataForecast= [];
  isFiveDaysCharged: boolean = false;
  showFiveDaysErrors: boolean = false;
  fiveDayserror;

  constructor(private ip: IpServiceService,
              private locationService: LocationService,
              private forecastService: ForecastService) { }

  ngOnInit(): void {
    this.getIP();
    moment.locale('es');
  }

  getIP() {
    this.ip.getIPAddress().subscribe((res:any) => {
      this.ipAddress = res.ip;
      this.getLocation();
    });
  }

  getLocation() {
    this.locationService.getLocation(this.ipAddress).subscribe((res:any) => {
      this.ubicationData = res;
      this.dataCharge = true;
      this.getForecastByCity();
    });
  }

  async getForecastByCity() {
    try {
      const forecastByIP = await this.forecastService.getForecastByIp(this.ipAddress);
      this.dataForecast = forecastByIP['list'];
      this.dataForecast.filter((data) => {
        data.dt_txt = moment(data.dt_txt).format('DD/MM/YYYY h:mm:ss a');
        //data.dt_txt = moment(data.dt_txt).format('dddd');
      });
      this.isFiveDaysCharged = true;
    } catch (error) {
      this.isFiveDaysCharged = false;
      this.showFiveDaysErrors = true;
      this.fiveDayserror = error;
      console.log(error)
    }
  }

}
