import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { CurrentService } from '../../services/current.service';
import { ForecastService } from '../../services/forecast.service';
import * as moment from 'moment';

@Component({
  selector: 'app-current',
  templateUrl: './current.component.html',
  styleUrls: ['./current.component.scss']
})
export class CurrentComponent implements OnInit {

  form: FormGroup;
  isCharged: boolean = false;
  isFiveDaysCharged: boolean = false;
  showResults: boolean = false;
  showErrors: boolean = false;
  error;
  showFiveDaysErrors: boolean = false;
  fiveDayserror;
  dataByCity = {};
  dataForecast= [];

  constructor(private fb: FormBuilder,
              private currentService: CurrentService,
              private forecastService: ForecastService) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      city: ['', Validators.required]
    });
    moment.locale('es');
  }

  get f() { return this.form.controls; }

  async getCurrentByCity() {
    if (this.form.invalid) {
      return;
    }
    this.isCharged = false;
    try {
      const resDataByCity = await this.currentService.getCurrentByCity(this.form.value);
      this.dataByCity = resDataByCity;
      this.isCharged = true;
      this.showResults = true;
      this.getForecastByCity();
    } catch (error) {
      this.showResults = false;
      this.showErrors = true;
      this.error = error;
      console.log(error)
    }
  }

  async getForecastByCity() {
    if (this.form.invalid) {
      return;
    }
    this.isFiveDaysCharged = false;
    try {
      const forecastByCity = await this.forecastService.getForecastByCity(this.form.value);
      this.dataForecast = forecastByCity['list'];
      console.log(this.dataForecast)
      this.dataForecast.filter((data) => {
        data.dt_txt = moment(data.dt_txt).format('DD/MM/YYYY h:mm:ss a');
        //data.dt_txt = moment(data.dt_txt).format('dddd');
      });
      this.isFiveDaysCharged = true;
    } catch (error) {
      this.isFiveDaysCharged = false;
      this.showFiveDaysErrors = true;
      this.fiveDayserror = error;
      console.log(error)
    }
  }

}
