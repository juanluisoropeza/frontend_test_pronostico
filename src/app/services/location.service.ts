import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap } from 'rxjs/operators';

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  initialPath: string = `${environment.backend}v1/`;

  constructor(private http:HttpClient) { }

  public getLocation(ip) {
    const data = {
      ip
    };
    return this.http.post<any>(this.initialPath+`location`, data)
      .pipe(tap(data => {
        return data;
      }));
  }

}
