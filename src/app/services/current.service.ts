import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CurrentService {

  initialPath: string = `${environment.backend}v1/`;

  constructor(private http:HttpClient) { }

  async getCurrentByCity(data) {
    const city = data.city;
    return await this.http.get(this.initialPath+`current/${city}`).toPromise();
  }

}
