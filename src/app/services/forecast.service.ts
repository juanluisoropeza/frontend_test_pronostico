import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ForecastService {

  initialPath: string = `${environment.backend}v1/`;

  constructor(private http:HttpClient) { }

  async getForecastByCity(data) {
    const city = data.city;
    return await this.http.post(this.initialPath+`forecast/${city}`, {}).toPromise();
  }

  async getForecastByIp(data) {
    const dataFormated = {
      ip: data
    };
    console.log(dataFormated);
    return await this.http.post(this.initialPath+`forecast/`, dataFormated).toPromise();
  }
}
