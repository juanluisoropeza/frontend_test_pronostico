import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { CurrentComponent } from './components/current/current.component';
import { LocationComponent } from './components/location/location.component';


const routes: Routes = [
  {
    path: '',
    component: LocationComponent
  },
  {
    path: 'location',
    component: LocationComponent
  },
  {
    path: 'current',
    component: CurrentComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
